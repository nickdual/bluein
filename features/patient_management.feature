Feature: Patient management
In order to identify and track patients admitted at my site
As a staff member with patient admissions duties
I want to easily enroll and discharge patients from the site

Background:
Given a user with patient admissions permissions exists
And user is logged in

Scenario: New patient search
When I go to the patients page
And I click on new patient search
Then I should see the patient search page
When I enter some patient details
And I click on search
Then I should see the search results

Scenario: Register new patient
Given a patient with similar demographics exists
And I perform a new patient search
Then I should see a notification of multiple results
When I enter further details
Then I should see a confirmation of only one patient found

Scenario: No known patient
Given a previously unknown patient exists
And I perform a new patient search unsuccessfully
Then I should see an option to apply for a new unique identifier

Scenario: Admit new patient
Given a patient has been found
And I click on new visit
Then I should see the new visit form
When I fill in the form
Then I should see confirmation of the new visit

Scenario: Discharge patient
When I go to the patients page
And I click discharge
Then I should see the discharge form
When I fill in the discharge form
Then I should see 'patient discharged'

Scenario: Transfer patient
When I go to the patients page
And I click transfer
Then I should see the transfer form
When I fill in the transfer form
Then I should see 'patient transferred'