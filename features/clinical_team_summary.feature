Feature: Clinical team summary and statistics
In order to be aware of my team's workload and situation
As a clinical team user
I need to see what positions I'm holding, how many patients I have, and other important statistics

Scenario: Summary on login
Given a user account holding several positions exists
And a few patients are admitted to their clinical team
When I go to the login page
And I enter my credentials
Then I should see my positions
And when I click on statistics
Then I should see the correct number of patients