Feature: Patient Results review
In order to keep up to date with my patients' laboratory results
As a lab results user
I need to see all lab results for each patient

Scenario: Results table
Given a patient exists with multiple results
And I open the patient chart
And I click on the results tab
Then I should see a table showing recent results

Scenario: Results graph
Given a patient exists with multiple results
And I open the patient chart
And I click on the results tab
And I select the values I am interested in
And I click on the graph tab
Then I should see a graph, with multiple stacked axes and highlighted normal ranges

Scenario: Urgent result
Given a patient exists on my clinical team
And a critically abnormal or unexpected value arrives
Then I should receive an urgent task to acknowledge this result
And a link to the patient results