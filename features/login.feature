Feature: Welcome page
In order to keep up to date with vital configuration options
As a logged-in user
I want to see my current account settings

Scenario: Successful login
Given a valid user account exists
When I go to the login page
And I enter my credentials
Then I should see 'welcome'

Scenario: Multiple sites
Given two different sites exist
And I am a member of both sites
When I go to the login page
And I enter my credentials
Then I should see the site name
When I go to the account settings page
Then I should see a menu offering to switch sites