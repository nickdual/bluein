Feature: All ordered results review
In order to ensure no results are missed and meet legal obligations
As a results ordering user
I be able to list all results that I've ordered, even for patients that have been transferred
I should see able to sign off the results and have this permanently logged