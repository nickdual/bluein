Feature: Site announcements and summary statistics
In order to be aware of broader issues affecting my site
As any user
I need to see a page showing site statistics and important announcements
