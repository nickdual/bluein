Given /^a user account holding several positions exists$/ do
  @user = FactoryGirl.create(:login_user)
  @clinical_team = FactoryGirl.create(:clinical_team, site: @user.site)
  @clinical_team.positions << FactoryGirl.create(:position, name: "Pirate", user: @user)
  @clinical_team.positions << FactoryGirl.create(:position, name: "Matey", user: @user)
end

Given /^a few patients are admitted to their clinical team/ do
  3.times do 
    patient = FactoryGirl.create(:patient) 
    patient.visits << FactoryGirl.create(:visit, clinical_team: @clinical_team)
  end
end

Then /^I should see my positions$/ do
  assert page.should have_content 'Pirate and Matey'
end

Then(/^when I click on statistics$/) do
  click_link 'Statistics'
end

Then /^I should see the correct number of patients$/ do
  assert page.should have_content 'You have 3 patients'
end

