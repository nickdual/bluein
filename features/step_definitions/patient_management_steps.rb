## Background
Given /^a user with patient admissions permissions exists$/ do
  user = FactoryGirl.create(:login_user)
  user.user_groups.first.roles = [ :patient_admissions ]
end

Given /^user is logged in$/ do
  visit new_user_session_url
  fill_in 'user_login', with: 'ik'
  fill_in 'user_password', with: 'secret'
  click_button 'Sign in'
end


## Search
When /^I go to the patients page$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I click on new patient search$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see the patient search page$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I enter some patient details$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I click on search$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see the search results$/ do
  pending # express the regexp above with the code you wish you had
end

## Register new patient

Given /^a patient with similar demographics exists$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I perform a new patient search$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see a notification of multiple results$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I enter further details$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see a confirmation of only one patient found$/ do
  pending # express the regexp above with the code you wish you had
end

## No known patient

Given /^a previously unknown patient exists$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I perform a new patient search unsuccessfully$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see an option to apply for a new unique identifier$/ do
  pending # express the regexp above with the code you wish you had
end

## Admit new patient

Given /^a patient has been found$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I click on new visit$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see the new visit form$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I fill in the form$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see confirmation of the new visit$/ do
  pending # express the regexp above with the code you wish you had
end

## Discharge patient

When /^I click discharge$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see the discharge form$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I fill in the discharge form$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see 'patient discharged'$/ do
  pending # express the regexp above with the code you wish you had
end

## Transfer patient

When /^I click transfer$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see the transfer form$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I fill in the transfer form$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see 'patient transferred'$/ do
  pending # express the regexp above with the code you wish you had
end
