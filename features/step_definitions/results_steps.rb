Given /^a patient exists$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I open the patient chart$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I click on the results tab$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see a table showing recent results$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^a patient exists with multiple results$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I select the values I am interested in$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I click on the graph tab$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should see a graph, with multiple stacked axes and highlighted normal ranges$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^a patient exists on my clinical team$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^a critically abnormal or unexpected value arrives$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should receive an urgent task to acknowledge this result$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^a link to the patient results$/ do
  pending # express the regexp above with the code you wish you had
end
