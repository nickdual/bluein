# Successful login

Given /^a valid user account exists$/ do
  FactoryGirl.create(:login_user)
end

When /^I go to the login page$/ do
  visit new_user_session_path
end

When /^I enter my credentials$/ do
  fill_in 'user_login', with: 'ik'
  fill_in 'user_password', with: 'secret'
  click_button 'Sign in'
end

Then /^I should see 'welcome'$/ do
  assert page.should have_content 'Welcome'
end

# Multiple sites

Given /^two different sites exist$/ do
  @site1 = FactoryGirl.create(:site, name: 'Pirate bay')
  @site2 = FactoryGirl.create(:site, name: 'Skull island')
end

Given /^I am a member of both sites$/ do  
  @group1 = FactoryGirl.create(:user_group, name: 'pirates', site: @site1, roles: [ :piracy ]) 
  @group2 = FactoryGirl.create(:user_group, name: 'seamen', site: @site2, roles: [ :piracy ])
  FactoryGirl.build(:user, name: 'test', login: 'ik', email: 'a@aol.com', password: 'secret', site: @site1,
                    user_groups: [ @group1, @group2 ]).save #have to build then save to prevent validation errors
end

Then /^I should see the site name$/ do
  assert page.should have_content 'You are logged in to Pirate bay'
end

When /^I go to the account settings page$/ do
  click_link 'Change settings'
end

Then /^I should see a menu offering to switch sites$/ do
  assert page.should have_content 'Pirate bay'
  assert page.should have_content 'Skull island'
end
