# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

puts 'EMPTY THE MONGODB DATABASE'
Mongoid.default_session.collections.each { |coll| coll.drop unless /^system/.match(coll.name) }
puts 'SETTING UP DEFAULT USER LOGIN'
@site = Site.create! name: 'BlueInfoMed'

@admin = @site.user_groups.create! name: 'Administrators', 
    roles: [ :super_user ]
@ilya = User.create! login: 'admin', name: 'Administrator',  email: 'user@example.com', password: 'secret', site: @site, user_groups: [@admin]  
puts 'New user created: ' << @ilya.name

@patient = Patient.create!  gender: "male", name: "First name" , date_of_birth: "2013-06-17 11:49:42"
@location = @site.locations.create!  department: "Benh Vien Quan 5", bed: "12"
@clinical_teams = @site.clinical_teams.create! name: "Clinical 1",specialty: "Specialty 1"
@visit = Visit.create! admission_time: "2013-06-17", discharge_time: "2013-06-30", location_id: @location.id, clinical_team_id: @clinical_teams.id, sensitive: false, patient_id: @patient.id
@note = @visit.notes.create!  text: "benh nhan nay bi benh gan", user_id: @ilya.id
@note = @visit.notes.create!  text: "benh nhan nay bi benh bao tu", user_id: @ilya.id
@note = @visit.notes.create!  text: "benh nhan nay bi benh tim", user_id: @ilya.id
@note = @visit.notes.create!  text: "benh nhan nay bi benh nao", user_id: @ilya.id
@note = @visit.notes.create!  text: "benh nhan nay bi benh phong", user_id: @ilya.id


