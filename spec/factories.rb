require 'factory_girl'

FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "User #{n}" }
    sequence(:login) { |n| "login#{n}" }
    sequence(:email) { |n| "user#{n}@aol.com" }
    password 'secret'
    site
    
    after(:build) do |user|
      user.user_groups << FactoryGirl.create(:user_group, site: user.site)
    end
    
    factory :login_user do
      login "ik"
      name "test user"
      email "a@aol.com"
      password "secret"
      site
    
      after(:build) do |user|
        user.user_groups << FactoryGirl.create(:user_group, site: user.site, roles: [:super_user])
      end
    end
  end

  factory :site do
    sequence(:name) { |n| "Site#{n}" }
  end
  
  factory :user_group do
    name 'New group'
    site
  end
  
  factory :patient do
    sequence(:name) { |n| "Patient#{n}" }
  end
  
  factory :visit do
    sequence(:admission_time) { |n| Time.now - n.weeks }
    clinical_team
    patient
    location
  end
  
  factory :location do
    department 'Emergency'
    site
  end
  
  factory :clinical_team do
    site
    name 'Pirates'
    specialty 'Piracy'
  end
  
  factory :position do
    name 'lackey'
    clinical_team
  end
  
  factory :address do
    type :work
    street1 '12 Shoalhaven St'
    city 'Nowra'
    country 'Australia'
    
    after(:build) do |address|
      patient = FactoryGirl.create(:patient)
      patient.contact.save
      patient.contact.addresses << address
    end
  end
  
  factory(:phone) do
    type :work
    country_code 64
    number 210327986
    
    after(:build) do |phone|
      patient = FactoryGirl.create(:patient)
      patient.contact.save
      patient.contact.phones << phone
    end
  end
  
  factory :note do
    text "blah"
    
    after(:build) do |note|
      FactoryGirl.create(:visit).notes << note
      note.user_id = FactoryGirl.create(:user)._id
    end
  end
end
