require 'spec_helper'

describe ClinicalTeam do
  let!(:clinical_team) { FactoryGirl.create(:clinical_team) }
  subject { clinical_team }
  
  it "should be valid when created from default factory" do
    should be_valid
  end
  
  it "should return appropriate patients" do
    patients = []
    3.times do
      visit = FactoryGirl.create(:visit, clinical_team: subject)
      patients << visit.patient
    end
    clinical_team.patients.should eq(patients)
  end
end
