require 'spec_helper'
require 'cancan/matchers'

describe 'User' do
  describe 'abilities' do
    let!(:user){ FactoryGirl.create(:user) }
    let!(:site) { user.site }
    let!(:user_group) { user.user_groups.first }
    let!(:another_user) { FactoryGirl.create(:user) }
    let!(:another_site) { another_user.site }
    let(:ability) { Ability.new(user) }
    subject { ability }
    
    context "new user" do
      before(:each) do 
        user_group.roles = [ :accounts ]
        user_group.save
      end
          
      it "can manage own account" do 
        subject.should be_able_to(:update, user)
        subject.should be_able_to(:read, user)
      end
      it "cannot delete own account" do
        should_not be_able_to(:delete, user)
      end
      it "cannot manage someone else's account" do
        another_user.site = another_site
        should_not be_able_to(:update, another_user)
      end
    end

    context "user_admin user" do
      before(:each) do 
        user_group.roles = [:user_admin]
        user_group.save
      end

      it "can manage accounts from own site" do 
        should be_able_to(:manage, user)
        should be_able_to(:delete, user)
        should be_able_to(:manage, user_group)
      end
    
      it "cannot update other sites' users" do
        another_user.site = another_site
        another_user.save
        should_not be_able_to(:update, another_user)
      end
    
      it "cannot manage other sites' user groups" do
        another_group = FactoryGirl.create(:user_group)
        another_group.site = another_site
        should_not be_able_to(:update, another_group)
      end
    end

    context "patient_admissions user" do
      let(:patient) { FactoryGirl.create(:patient) }
      let(:visit) { FactoryGirl.create(:visit) }
    
      before(:each) do
        user_group.roles = [ :patient_admissions ]
        user_group.save
        visit.location.site = site
      end
    
      it { should be_able_to(:update, patient) }
      it { should be_able_to(:manage, visit) }
      it "cannot admit to another site" do
        another_visit = FactoryGirl.create(:visit)
        another_visit.clinical_team.site = another_site
        should_not be_able_to(:update, another_visit)
      end
    end
  end
end