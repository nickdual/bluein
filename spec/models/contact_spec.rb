require 'spec_helper'

describe Contact do
  it "is auto-loaded in new patient" do
    patient = FactoryGirl.create(:patient)
    patient.contact.should be_valid
  end
end
