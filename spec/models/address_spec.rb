require 'spec_helper'

describe Address do
  
  def valid_attributes
    {
      type: :work,
      street1: '12 Shoalhaven St',
      city: 'Nowra',
      country: 'Australia'
    }
  end
  
  let(:patient) { FactoryGirl.create(:patient) }
  let(:user) { FactoryGirl.create(:user) }
  
  it "polymorphism and validation works" do
    patient.contact.save
    patient.contact.addresses.create! valid_attributes
    patient.contact.addresses.last.should be_valid
    user.contact.save
    user.contact.addresses.create! valid_attributes
    user.contact.addresses.last.should be_valid
  end
end
