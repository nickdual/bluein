require 'spec_helper'

describe Patient do
  subject { patient }
  let!(:patient) { FactoryGirl.create(:patient) }
  
  it "should be valid when created from default factory" do
    patient.should be_valid
  end
  
  it "should return current_visit and notes appropriately" do
    visit = FactoryGirl.create(:visit, patient: patient)
    visit.notes << FactoryGirl.create(:note)
    note = visit.notes.last
    
    patient.current_visit.should eq(visit)
    patient.notes.should eq([note])
  end
end
