require 'spec_helper'

describe Phone do
  def valid_attributes
    FactoryGirl.attributes_for(:phone)
  end
  
  let(:patient) { FactoryGirl.create(:patient) }
  let(:user) { FactoryGirl.create(:user) }
  
  it "polymorphism and validation works" do
    patient.contact.save
    patient.contact.phones.create! valid_attributes
    patient.contact.phones.last.should be_valid
    user.contact.save
    user.contact.phones.create! valid_attributes
    user.contact.phones.last.should be_valid
  end
  
end
