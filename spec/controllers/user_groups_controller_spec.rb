require 'spec_helper'

describe UserGroupsController do

  def valid_attributes
    FactoryGirl.attributes_for(:user_group).merge site_id: site._id
  end
  
  let(:site) { user_group.site }
  let!(:user_group) { FactoryGirl.create(:user_group) }
  
  before(:each) do
    login_user
  end

  describe "GET index" do
    it "assigns all user_groups as @user_groups" do
      get :index, { site_id: site._id }
      assigns(:user_groups).should eq([ user_group ])
    end
  end

  describe "GET show" do
    it "assigns the requested user_group as @user_group" do
      get :show, {:id => user_group.to_param, site_id: site._id }
      assigns(:user_group).should eq(user_group)
    end
  end

  describe "GET new" do
    it "assigns a new user_group as @user_group" do
      get :new, { site_id: site._id }
      assigns(:user_group).should be_a_new(UserGroup)
    end
  end

  describe "GET edit" do
    it "assigns the requested user_group as @user_group" do
      get :edit, {:id => user_group.to_param, site_id: site._id }
      assigns(:user_group).should eq(user_group)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new UserGroup" do
        expect {
          post :create, {user_group: valid_attributes, site_id: site._id  }
        }.to change(UserGroup, :count).by(1)
      end

      it "assigns a newly created user_group as @user_group" do
        post :create, {:user_group => valid_attributes, site_id: site._id }
        assigns(:user_group).should be_a(UserGroup)
        assigns(:user_group).should be_persisted
      end

      it "redirects to the created user_group" do
        post :create, {:user_group => valid_attributes, site_id: site._id }
        response.should redirect_to(site_user_group_url(site._id, UserGroup.last))
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved user_group as @user_group" do
        # Trigger the behavior that occurs when invalid params are submitted
        UserGroup.any_instance.stub(:save).and_return(false)
        post :create, {:user_group => {}, site_id: site._id }
        assigns(:user_group).should be_a_new(UserGroup)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        UserGroup.any_instance.stub(:save).and_return(false)
        post :create, {:user_group => {}, site_id: site._id }
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested user_group" do
        # Assuming there are no other user_groups in the database, this
        # specifies that the UserGroup created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        UserGroup.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
        put :update, {:id => user_group.to_param, :user_group => {'these' => 'params'}, site_id: site._id }
      end

      it "assigns the requested user_group as @user_group" do
        put :update, {:id => user_group._id, :user_group => valid_attributes, site_id: site._id }
        assigns(:user_group).should eq(user_group)
      end

      it "redirects to the user_group" do
        put :update, {:id => user_group.to_param, :user_group => valid_attributes, site_id: site._id }
        response.should redirect_to(site_user_group_url(site._id, user_group._id))
      end
    end

    describe "with invalid params" do
      it "assigns the user_group as @user_group" do
        # Trigger the behavior that occurs when invalid params are submitted
        UserGroup.any_instance.stub(:save).and_return(false)
        put :update, {:id => user_group.to_param, :user_group => {}, site_id: site._id }
        assigns(:user_group).should eq(user_group)
      end

      it "re-renders the 'edit' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        UserGroup.any_instance.stub(:save).and_return(false)
        put :update, {:id => user_group.to_param, :user_group => {}, site_id: site._id }
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested user_group" do
      expect {
        delete :destroy, {:id => user_group.to_param, site_id: site._id }
      }.to change(UserGroup, :count).by(-1)
    end

    it "redirects to the user_groups list" do
      delete :destroy, {:id => user_group.to_param, site_id: site._id }
      response.should redirect_to(site_user_groups_url(site._id))
    end
  end

end
