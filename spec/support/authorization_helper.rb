module ValidUserHelper
  def login_user
    @request.env["devise.mapping"] = Devise.mappings[:user]
    current_user = FactoryGirl.create(:login_user)
    sign_in current_user
  end
end

RSpec.configure do |config|
  config.include ValidUserHelper, type: :controller
end