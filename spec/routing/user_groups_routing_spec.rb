require "spec_helper"

describe UserGroupsController do
  describe "routing" do

    it "routes to #index" do
      get("/sites/1/user_groups").should route_to("user_groups#index", site_id: "1")
    end

    it "routes to #new" do
      get("/sites/1/user_groups/new").should route_to("user_groups#new", site_id: "1")
    end

    it "routes to #show" do
      get("/sites/1/user_groups/1").should route_to("user_groups#show", :id => "1", site_id: "1")
    end

    it "routes to #edit" do
      get("/sites/1/user_groups/1/edit").should route_to("user_groups#edit", :id => "1", site_id: "1")
    end

    it "routes to #create" do
      post("/sites/1/user_groups").should route_to("user_groups#create", site_id: "1")
    end

    it "routes to #update" do
      put("/sites/1/user_groups/1").should route_to("user_groups#update", :id => "1", site_id: "1")
    end

    it "routes to #destroy" do
      delete("/sites/1/user_groups/1").should route_to("user_groups#destroy", :id => "1", site_id: "1")
    end

  end
end
