require "spec_helper"

describe NotesController do
  describe "routing" do

    it "routes to #index" do
      get("/patients/1/notes").should route_to("notes#index", patient_id: "1")
    end

    it "routes to #new" do
      get("/patients/1/visits/1/notes/new").should route_to("notes#new", patient_id: "1", visit_id: "1")
    end

    it "routes to #show" do
      get("/patients/1/visits/1/notes/1").should route_to("notes#show", :id => "1", patient_id: "1", visit_id: "1")
    end

    it "routes to #edit" do
      get("/patients/1/visits/1/notes/1/edit").should route_to("notes#edit", :id => "1", patient_id: "1", visit_id: "1")
    end

    it "routes to #create" do
      post("/patients/1/visits/1/notes").should route_to("notes#create", patient_id: "1", visit_id: "1")
    end

    it "routes to #update" do
      put("/patients/1/visits/1/notes/1").should route_to("notes#update", :id => "1", patient_id: "1", visit_id: "1")
    end

    it "routes to #destroy" do
      delete("/patients/1/visits/1/notes/1").should route_to("notes#destroy", :id => "1", patient_id: "1", visit_id: "1")
    end

  end
end
