require "spec_helper"

describe ClinicalTeamsController do
  describe "routing" do

    it "routes to #index" do
      get("/clinical_teams").should route_to("clinical_teams#index")
    end

    it "routes to #new" do
      get("/clinical_teams/new").should route_to("clinical_teams#new")
    end

    it "routes to #show" do
      get("/clinical_teams/1").should route_to("clinical_teams#show", :id => "1")
    end

    it "routes to #edit" do
      get("/clinical_teams/1/edit").should route_to("clinical_teams#edit", :id => "1")
    end

    it "routes to #create" do
      post("/clinical_teams").should route_to("clinical_teams#create")
    end

    it "routes to #update" do
      put("/clinical_teams/1").should route_to("clinical_teams#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/clinical_teams/1").should route_to("clinical_teams#destroy", :id => "1")
    end

  end
end
