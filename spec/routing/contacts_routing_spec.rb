require "spec_helper"

describe ContactsController do
  describe "routing" do

    it "routes to #show" do
      get("/patients/1/contact/").should route_to("contacts#show", :patient_id => "1")
    end

    it "routes to #edit" do
      get("/patients/1/contact/edit").should route_to("contacts#edit", :patient_id => "1")
    end

    it "routes to #update" do
      put("/patients/1/contact/").should route_to("contacts#update", :patient_id => "1")
    end

  end
end
