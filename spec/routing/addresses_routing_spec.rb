require "spec_helper"

describe AddressesController do
  describe "routing" do

    it "routes to #new" do
      get("/patients/1/contact/addresses/new").should route_to("addresses#new", patient_id: "1")
    end

    it "routes to #edit" do
      get("/patients/1/contact/addresses/1/edit").should route_to("addresses#edit", :id => "1", patient_id: "1")
    end

    it "routes to #create" do
      post("/patients/1/contact/addresses").should route_to("addresses#create", patient_id: "1")
    end

    it "routes to #update" do
      put("/patients/1/contact/addresses/1").should route_to("addresses#update", :id => "1", patient_id: "1")
    end

    it "routes to #destroy" do
      delete("/patients/1/contact/addresses/1").should route_to("addresses#destroy", :id => "1", patient_id: "1")
    end

  end
end
