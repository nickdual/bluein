require "spec_helper"

describe PositionsController do
  describe "routing" do

    it "routes to #index" do
      get("/clinical_teams/1/positions").should route_to("positions#index", clinical_team_id: "1")
    end

    it "routes to #new" do
      get("/clinical_teams/1/positions/new").should route_to("positions#new", clinical_team_id: "1")
    end

    it "routes to #show" do
      get("/clinical_teams/1/positions/1").should route_to("positions#show", :id => "1", clinical_team_id: "1")
    end

    it "routes to #edit" do
      get("/clinical_teams/1/positions/1/edit").should route_to("positions#edit", :id => "1", clinical_team_id: "1")
    end

    it "routes to #create" do
      post("/clinical_teams/1/positions").should route_to("positions#create", clinical_team_id: "1")
    end

    it "routes to #update" do
      put("/clinical_teams/1/positions/1").should route_to("positions#update", :id => "1", clinical_team_id: "1")
    end

    it "routes to #destroy" do
      delete("/clinical_teams/1/positions/1").should route_to("positions#destroy", :id => "1", clinical_team_id: "1")
    end

  end
end
