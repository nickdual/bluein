require "spec_helper"

describe VisitsController do
  describe "routing" do

    it "routes to #index" do
      get("/patients/1/visits").should route_to("visits#index", patient_id: "1")
    end

    it "routes to #new" do
      get("/patients/1/visits/new").should route_to("visits#new", patient_id: "1")
    end

    it "routes to #show" do
      get("/patients/1/visits/1").should route_to("visits#show", :id => "1", patient_id: "1")
    end

    it "routes to #edit" do
      get("/patients/1/visits/1/edit").should route_to("visits#edit", :id => "1", patient_id: "1")
    end

    it "routes to #create" do
      post("/patients/1/visits").should route_to("visits#create", patient_id: "1")
    end

    it "routes to #update" do
      put("/patients/1/visits/1").should route_to("visits#update", :id => "1", patient_id: "1")
    end

    it "routes to #destroy" do
      delete("/patients/1/visits/1").should route_to("visits#destroy", :id => "1", patient_id: "1")
    end

  end
end
