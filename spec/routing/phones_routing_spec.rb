require "spec_helper"

describe PhonesController do
  describe "routing" do

    it "routes to #new" do
      get("/patients/1/contact/phones/new").should route_to("phones#new", patient_id: "1")
    end

    it "routes to #edit" do
      get("/patients/1/contact/phones/1/edit").should route_to("phones#edit", :id => "1", patient_id: "1")
    end

    it "routes to #create" do
      post("/patients/1/contact/phones").should route_to("phones#create", patient_id: "1")
    end

    it "routes to #update" do
      put("/patients/1/contact/phones/1").should route_to("phones#update", :id => "1", patient_id: "1")
    end

    it "routes to #destroy" do
      delete("/patients/1/contact/phones/1").should route_to("phones#destroy", :id => "1", patient_id: "1")
    end


  end
end
