module UserGroupsHelper
  
  private
  # This function is needed to reformat the strings returned from forms into the correct objects/symbols for the model
  def get_group_params(old_params)
    group_params = old_params[:user_group]
    
    if(!group_params[:roles].nil?)
      group_params[:roles].delete("")  # remove the "" string from the end which seems to get added on to the array
      group_params[:roles].collect! { |role| role.to_sym }
    end
    
    #group_params[:user_ids].delete("") unless group_params[:user_ids].nil?
    #if(!group_params[:site].nil? && group_params[:site].empty?)
    #  group_params[:site_id] = nil
    #end
    
    group_params
  end
end