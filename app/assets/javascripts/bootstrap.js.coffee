APP.init = ->
  $("a[rel=popover]").popover()
  $(".popover").popover()
  $(".tooltip").tooltip()
  $("a[rel=tooltip]").tooltip()
  $(".datechooser input").datepicker({"format": "yyyy-mm-dd", "weekStart": 1, "autoclose": true})
  $(".sortable").tablesorter()
  $(".section-body").collapse()
  $(".multiselect").select2()

  paths = location.pathname.split("/")
  if paths.length > 1
    path = paths[1]    
    $("#" + path).addClass("active") # highlight top navbar element
    
    if path == "patients" # patients_chart layout sidebar
      if paths.length > 3
        if paths[3] == "visits" && paths.length > 5
          $("#notes").addClass("active")
        else
          $("#" + paths[3]).addClass("active")
      if paths.length == 3
        $("#demographics").addClass("active")
        
    if path == "sites" # sites_chart layout sidebar
      if paths.length == 3
        $("#summary").addClass("active")
      if paths.length > 3
        $("#" + paths[3]).addClass("active")
    
    if path == "users" # users_chart layout sidebar
      if paths.length == 3
        $("#summary").addClass("active")
      if paths.length > 3
        $("#" + paths[3]).addClass("active")