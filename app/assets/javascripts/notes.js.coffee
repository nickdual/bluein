# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(document).ready () ->
  $.widget "custom.catcomplete", $.ui.autocomplete,
    _renderMenu: (ul, items) ->
      ul.addClass('arrow_box_top')
      that = this
      type = ""
      $.each items, (index, item) ->
        unless item.type is type
          ul.append "<li class='ui-autocomplete-category'>" + item.category + "</li>"
          type = item.type
        that._renderItemData ul, item
  $("#search_key").catcomplete({
    autoFocus: true,
    delay: 0,
    source: "/home/suggestion",

    select: ( event, ui ) ->
      console.log(ui)
      $("#search_type").val(ui.item.type)
    open: () ->
      position = $(".ui-autocomplete.arrow_box_top").position()
      top = position.top
      $(".ui-autocomplete.arrow_box_top").css({top: (top + 10) + "px" })
  })
