# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

APP.home =
  stats: ->
    $("#pie").highcharts
      chart:
        plotBackgroundColor: null
        plotBorderWidth: null
        plotShadow: false

      title:
        text: null
      
      credits:
        enabled: false

      tooltip:
        pointFormat: "{series.name}: <b>{point.percentage}%</b>"
        percentageDecimals: 1

      plotOptions:
        pie:
          allowPointSelect: true
          cursor: "pointer"
          dataLabels:
            enabled: true
            color: "#000000"
            connectorColor: "#000000"
            formatter: ->
              "<b>" + @point.name + "</b>: " + @percentage + " %"

      series: [
        type: "pie"
        name: "Admission share"
        data: [["Medical", 45.0], ["Surgical", 45.0],
          name: "Yours"
          y: 10
          sliced: true
          selected: true
        ]
      ]



