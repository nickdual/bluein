# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

obschart = ->

	TempChart = new Highcharts.Chart (
	  chart:
	    renderTo: "temp-chart"
		
			title:
				text: null
			
	  xAxis:
	    labels:
	      enabled: false

	  yAxis:
	    title:
	      text: "Temp (°C)"

	    labels:
	      x: -15

	    plotBands: [
	      from: 0
	      to: 35
	      color: "rgba(68, 170, 213, 0.1)"
	    ,
	      from: 37.5
	      to: 50
	      color: "rgba(68, 170, 213, 0.1)"
	    ]
	    endOnTick: false
	    tickInterval: 1
	    maxPadding: 0.1

	  legend:
	    enabled: false

	  credits:
	    enabled: false

	  plotOptions:
	    line:
	      pointStart: 1940

	  tooltip:
	    crosshairs: true

	  series: [
	    name: "Temperature"
	    data: [37, 37, 37, 37, 37, 36, 37, 37, 37, 37, 38, 37.5, 37, 37.3, 37, 37, 37, 37.9, 37, 37, 37]
	  ]
	)

	BPHRChart = new Highcharts.Chart(
	  chart:
	    renderTo: "bphr-chart"
	    defaultSeriesType: "area"
	    alignTicks: false

	  title:
	    text: null

	  xAxis:
	    labels:
	      enabled: false

	  yAxis:
	    title:
	      text: "Blood Pressure / Heart Rate"

	    id: 0
	    min: 0
	    max: 200
	    tickInterval: 20
	    plotBands: [
	      from: 0
	      to: 60
	      color: "rgba(68, 170, 213, 0.1)"
	    ,
	      from: 140
	      to: 200
	      color: "rgba(68, 170, 213, 0.1)"
	    ]

	  tooltip:
	    shared: true
	    crosshairs: true
	    formatter: ->
	      "<b>" + @x + "</b><br />" + "BP: " + (@points[1].y + @points[0].y) + "/" + @points[1].y + "<br />" + "HR: " + @points[2].y

	  legend:
	    enabled: false

	  credits:
	    enabled: false

	  plotOptions:
	    area:
	      pointStart: 1940
	      marker:
	        enabled: false
	        symbol: "circle"
	        radius: 2
	        states:
	          hover:
	            enabled: true

	      lineWidth: 0
	      stacking: "normal"

	    line:
	      pointStart: 1940

	  series: [

	    # this series defines the height of the range
	    name: "Blood pressure"
	    data: [100, 80, 100, 90, 100, 80, 80, 70, 80, 40, 50, 100, 80, 60, 65, 70, 100, 110, 120, 120, 120]
	    stack: 1
	  ,

	    # this series defines the bottom values
	    name: "Diastolic"
	    data: [70, 80, 80, 60, 40, 90, 80, 80, 100, 80, 80, 80, 70, 50, 30, 20, 10, 30, 80, 80, 80]
	    enableMouseTracking: true
	    showInLegend: false
	    fillColor: "rgba(255, 255, 255, 0)"
	    stack: 1
	  ,
	    name: "Heart rate"
	    data: [50, 60, 90, 70, 90, 70, 89, 99, 79, 88, 66, 99, 77, 120, 130, 140, 150, 130, 120, 140, 120]
	    stack: 0
	    type: "line"
	  ]
	)
	
	RespChart = new Highcharts.Chart(
	  chart:
	    renderTo: "resp-chart"

	  title:
	    text: null

	  xAxis: {}
	  yAxis: [
	    title:
	      text: "Respiratory"

	    labels:
	      x: -15

	    plotBands: [
	      from: 0
	      to: 8
	      color: "rgba(68, 170, 213, 0.1)"
	    ,
	      from: 18
	      to: 100
	      color: "rgba(68, 170, 213, 0.1)"
	    ]
	    id: 0
	    endOnTick: false
	    tickInterval: 2
	    maxPadding: 0.1
	  ,
	    title:
	      text: null

	    labels:
	      enabled: false

	    id: 1
	    max: 100
	    tickInterval: 2
	    opposite: true
	  ]
	  legend:
	    enabled: false

	  credits:
	    enabled: false

	  plotOptions:
	    line:
	      pointStart: 1940

	  tooltip:
	    crosshairs: true
	    shared: true

	  series: [
	    name: "RR"
	    data: [18, 16, 20, 16, 16, 16, 16, 16, 16, 16, 24, 25, 14, 15, 16, 15, 16, 19, 14, 14, 16]
	  ,
	    name: "SpO2"
	    data: [99, 99, 99, 99, 99, 99, 99, 99, 99, 95, 85, 89, 94, 99, 99, 99, 99, 99, 99, 99, 99]
	    yAxis: 1
	    dashStyle: "shortdot"
	  ]
	)