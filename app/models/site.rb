class Site
  include Mongoid::Document
  
  field :name, type: String
  field :abbreviation, type: String, default: ""
  
  has_many :user_groups
  has_many :users
  has_many :locations
  has_many :clinical_teams
  
  validates_presence_of :name
  validates_uniqueness_of :name
  
  def visits
    Visit.where( :clinical_team.in => clinical_team_ids)
  end
  def visit_ids
    @visits = Visit.where( :clinical_team.in => clinical_team_ids)
    visit_ids = []
    if  @visits.length != 0
      @visits.each do |visit|
        visit_ids.push(visit.id.to_s)
      end
    end
    return visit_ids.to_a
  end

end