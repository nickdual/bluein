class Phone
  include Mongoid::Document
  include ActionView::Helpers::NumberHelper
  
  field :type, type: String
  field :name, type: String
  field :country_code, type: Integer
  field :number, type: String
  field :extension, type: String
  
  embedded_in :contact
  
  validates_presence_of :type, :country_code, :number
  validates_numericality_of :country_code, :number
  
  def formatted
    number_to_phone(number, area_code: true, country_code: country_code, extension: extension)
  end
end
