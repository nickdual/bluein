class ClinicalTeam
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include Mongoid::Timestamps::Updated

  field :name, type: String
  field :specialty, type: String
  field :consultant_id, type: Moped::BSON::ObjectId
  belongs_to :site
  has_many :visits
  has_many :positions
  
  validates_presence_of :name, :specialty
  
  def patients
    Patient.where(:_id.in => visits.active.distinct(:patient)) unless visits.empty? #ugly and slow?
  end
end