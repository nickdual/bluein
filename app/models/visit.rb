class Visit
  include Mongoid::Document
  include ActionView::Helpers::DateHelper

  field :admission_time, type: DateTime
  field :discharge_time, type: DateTime
  field :discharge_type, type: Symbol
  field :discharge_destination, type: String
  field :sensitive, type: Boolean, default: false
  
  belongs_to :patient
  belongs_to :location
  belongs_to :clinical_team
  has_many :notes
  
  validates_presence_of :patient, :location, :admission_time
  
  scope :active, where(discharge_time: nil)

  def length_of_stay
    if admission_time.present?
      return distance_of_time_in_words(admission_time, discharge_time || DateTime.now.to_date)
    else
      return "?"
    end
  end
  
  def active?
    discharge_time.nil?
  end
  
  def name
    if clinical_team.present?
      specialty = clinical_team.id.to_s
    else
      specialty = location.department
    end
    specialty + " (" + location.site.abbreviation + ") " + (I18n.l admission_time, format: :date_only)
  end
end