class Patient
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include Mongoid::Timestamps::Updated
  include ActionView::Helpers::DateHelper

  include Tire::Model::Search
  include Tire::Model::Callbacks

  field :name, type: String
  field :date_of_birth, type: Date
  field :gender, type: Symbol, default: :unknown
  
  has_many :visits
  embeds_one :contact, as: :contactable, autobuild: true
  
  validates_presence_of :name
  
  def age_in_words
    if date_of_birth.present?
      return distance_of_time_in_words_to_now(date_of_birth)
    else
      return 'unknown'
    end
  end
  
  def age
    now = Time.now.utc.to_date
    age = now.year - date_of_birth.year - 
          ((now.month > date_of_birth.month || (now.month == date_of_birth.month && now.day >= date_of_birth.day)) ? 0 : 1)
    if(age == 0)
      age = age_in_words
    end
    
    age
  end
  
  def current_visit
    visits.active.last
  end
  
  def notes
    current_visit.notes unless current_visit.nil?
  end
  
  def gender_symbol
    case gender
    when :male
      return "&#x2642;".html_safe
    when :female
      return "&#x2640;".html_safe
    when :unknown
      return "?"
    end
  end


  def to_indexed_json
    {
      :id => self.id,
      :name => self.name,
      :gender => self.gender,
      :visit_ids => self.visit_ids.blank? ? '': self.visit_ids.last
    }.to_json
  end

end
