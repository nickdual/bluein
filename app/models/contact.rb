class Contact
  include Mongoid::Document
  
  field :notes, type: String
    
  embeds_many :addresses
  embeds_many :phones
  embedded_in :contactable, polymorphic: true
end
