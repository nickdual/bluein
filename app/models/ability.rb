class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new 
    roles = user.roles_list
    site_id = user.site_id
    
    return if roles.empty?

    can [:read, :update, :login_record, :job_description], User, _id: user._id
    can :manage, Contact, :contactable => { _id: user._id } # ugly hack as contactable type not checked
    can :manage, Address, contact: { contactable: { _id: user._id } }
    can :manage, Phone, contact: { contactable: { _id: user._id } }
    can :read, Contact do |contact|
      contactable = contact.contactable
      case contactable.class
      when User
        contactable.site_id == site_id
      when Patient
        if contactable.current_visit.present?
          contactable.current_visit.location.site_id == site_id
        else
          false
        end
      end
    end
    can :read, ClinicalTeam, site_id: site_id
    can :read, Position, clinical_team: { site_id: site_id }
    can :read, Location, site_id: site_id
        
    roles.sort!
    roles.each do |role|
      case role
      when :bed_management # todo
      when :billing
      when :coding
      when :imaging_orders
      when :lab_orders
      when :metrics
      when :outpatient_bookings
        
      when :patient_admissions
        can :manage, Patient
        can :create, Visit # TODO: needs to be restricted to own site by param processing
        can :index, Visit, sensitive: false
        can :manage, Visit, location: { site_id: site_id }
        
      when :patient_records
        can :manage, Patient
        can :index, Visit, sensitive: false
        can :read, Visit, location: { site_id: site_id }
        can :manage, Note, visit: { location: { site_id: site_id } }
        
      when :super_user
        can :manage, :all
        
      when :staff_rosters
        can :manage, ClinicalTeam, site_id: site_id
        can :manage, Position, clinical_team: { site_id: site_id }
        can [:read, :update], User, site_id: site_id
        
      when :user_admin
        can :manage, UserGroup, site_id: site_id
        can :manage, User, site_id: site_id
        can :manage, ClinicalTeam, site_id: site_id
        can :manage, Position, clinical_team: { site_id: site_id }
        can :manage, Location, site_id: site_id
        can :read, Site, _id: site_id
        cannot [:edit, :update, :delete], UserGroup, roles: :super_user
        
      end
    end
  end
end
