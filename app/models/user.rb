class User
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :recoverable, rememberable, :registerable, and :omniauthable
  devise :database_authenticatable, :trackable, :validatable, :timeoutable, :lockable

  field :login, type: String
  field :name, type: String

  ## Database authenticatable
  field :email,              type: String
  field :encrypted_password, type: String

  ## Recoverable
  # field :reset_password_token,   :type => String
  # field :reset_password_sent_at, :type => Time

  ## Rememberable
  # field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Encryptable
  # field :password_salt, :type => String

  ## Confirmable
  # field :confirmation_token,   :type => String
  # field :confirmed_at,         :type => Time
  # field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  #field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  field :locked_at,       type: Time

  ## Token authenticatable
  # field :authentication_token, :type => String
  
  validates_presence_of :login, :name, :email, :user_groups, :site
  validates_uniqueness_of :login, :email, case_sensitive: false
  
  has_and_belongs_to_many :user_groups
  has_many :positions
  belongs_to :site # the current site
  
  embeds_one :contact, as: :contactable, autobuild: true
  
  # Called by Devise after authenticating a user's login details and before signing in
  # This is to change the user's site if their old site/group was changed or deleted
  def valid_for_authentication?
    if(site.nil? || !available_sites.include?(site)) # if never logged in before, or no longer a member of the current site
      if(user_groups.empty?) # if not in any user groups either
        return false
      end
      self.site = user_groups.first.site
    end
    return super && site.present?
  end
  
  def available_sites
    Site.find(user_groups.distinct(:site))
  end
  
  # Gives list of user roles for permission engine (previously written thus for CanTango)
  # TODO: cache this at login?
  def roles_list
    if(site.present?)
      return user_groups.where(site: site).distinct(:roles)
    end
    return []
  end
  
  def clinical_teams
    ClinicalTeam.where(:_id.in => positions.distinct(:clinical_team)) #ugly and slow?
  end
end