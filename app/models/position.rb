class Position
  include Mongoid::Document
  
  field :name, type: String
  field :superior, type: Moped::BSON::ObjectId
  
  belongs_to :clinical_team
  belongs_to :user
  
  validates_presence_of :name, :clinical_team
  
  scope :with_site, ->(site){ 
    clinical_team_ids = Site.find(site._id).clinical_team_ids
    where(:clinical_team.in => clinical_team_ids) # ugly manual join, may need to denormalise and embed at some point?
  }
end
