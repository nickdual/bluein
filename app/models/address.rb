class Address
  include Mongoid::Document
  
  field :type, type: Symbol
  field :name, type: String
  field :street1, type: String
  field :street2, type: String
  field :city, type: String
  field :state, type: String
  field :post_code, type: String
  field :country, type: String
  
  embedded_in :contact
  
  validates_presence_of :type, :street1, :city, :country
end
