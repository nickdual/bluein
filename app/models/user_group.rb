class UserGroup
  include Mongoid::Document
  
  field :name, type: String
  field :roles, type: Array, default: []
  
  belongs_to :site
  has_and_belongs_to_many :users
  
  validates_presence_of :site, :name
  
  def to_s
    name
  end
end
