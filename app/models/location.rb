class Location
  include Mongoid::Document
  
  field :department, type: String
  field :bed, type: String
  field :vacant, type: Boolean, default: true
  
  has_many :visits
  belongs_to :site
  
  validates_presence_of :department, :site
  
  scope :vacant, -> { where(vacant: true) }
  before_save do |location|
    location.vacant = !location.visits.exists?
    return true
  end

  def name
    department + " - " + bed
  end
end
