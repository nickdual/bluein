class Note
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include Mongoid::Timestamps::Updated
  include Tire::Model::Search
  include Tire::Model::Callbacks

  field :user_id, type: Moped::BSON::ObjectId
  field :text, type: String
  
  belongs_to :visit
  
  validates_presence_of :user_id

  def to_indexed_json
    {
        :id => self.id,
        :text => self.text,
        :user_id => self.user_id,
        :visit_id => self.visit_id
    }.to_json
  end

  def patient_name
    self.visit.patient.name
  end

  def author
    @user = User.find(self.user_id)
    return @user.name
  end
end