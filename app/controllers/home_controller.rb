class HomeController < ApplicationController
  skip_authorization_check only: [:public,:suggestion, :show_more]
  layout 'home_chart', only: [:index, :stats]
  def suggestion
    term = /#{params[:term]}/i
    @user = current_user
    authorize! :read, @user
    @site = current_user.site
    @visits = @site.visits
    @result = []

    @result_patient = Patient.where(:name => term).only(:name)
    @result_patient = @result_patient.map {|u| Hash[ label: " #{u.name.split.slice(0, 10).join(" ")}", value: u.name.to_s.split.slice(0, 10).join(" "),type: 'patient',category: 'Partients']}
    @result_patient = @result_patient.uniq
    @result_patient = @result_patient[0,3]
    @result +=  @result_patient

    @notes = []
    @visits.each do |visit|
      @result_note = visit.notes.where(:text => term).only(:text)
      @result_note = @result_note.map {|u| Hash[ label: "  #{u.text.split.slice(0, 10).join(" ")}", value: u.text.to_s.split.slice(0, 10).join(" "), type: 'note',category: "Notes containing"]}
      @notes += @result_note
    end
    @notes = @notes.uniq
    @notes = @notes[0,3]
    @result +=  @notes


    render :json => @result
  end

  def index
    @user = current_user
    authorize! :read, @user
    @site = current_user.site
    if params[:search_key].present?
      text =  params[:search_key].blank? ? '*' :"*" + params[:search_key].strip + "*"
      session[:search_key] = text
      session[:type] = params[:type] if params[:type]
      visit_ids = @site.visit_ids()
      size = 20
      from = params[:page].blank? ? 0 : params[:page]* size     #begin from page = 0
      if params[:type].blank? || params[:type] == 'note'
        @notes_search = Tire.search('notes',load: true) do
          from from
          size size
          query { string "text:#{text}" , :default_operator => 'AND'}
          filter :terms, :visit_id => visit_ids
          facet('visit_id') { terms :visit_ids }
        end
      end
      if params[:type].blank? || params[:type] == 'patient'
        @patients_search = Tire.search('patients',load: true) do
          from  from
          size  size
          query { string "name:#{text}" , :default_operator => 'AND'}
          filter :terms, :visit_ids => visit_ids
          facet('visit_ids') { terms :visit_ids }
        end
      end
    else
      if(flash[:notice] == "Signed in successfully.")
        flash[:notice] = nil
        @just_logged_in = true
      end

      positions_list = @user.positions.with_site(@site).collect{ |p| p.name }.to_sentence
      case @user.positions.with_site(@site).count
      when 0
        @positions = "You have no active positions. Get a real job!"
      when 1
        @positions = "Your position is currently <strong>".html_safe + positions_list + "</strong>.".html_safe
      else
        @positions = "Your positions are currently <strong>".html_safe + positions_list + "</strong>.".html_safe
      end
    end
  end
  
  def stats
    @user = current_user
    authorize! :read, @user
    @site = current_user.site
    
    @admissions_count = @site.visits.active.count unless @site.visits.nil?
    @patients_count = @user.clinical_teams.where(site: @site).collect{ |t| t.patients.nil? ? 0 : t.patients.count }.sum

    total_beds = @site.locations.count
    occupied_beds = total_beds - @site.locations.vacant.count
    @occupancy = occupied_beds.to_f / total_beds.to_f
  end
  
  def public
    @site_count = Site.all.count
    render layout: 'public'
  end

  def show_more
      puts session[:search_key]
      puts session[:type]
      @user = current_user
      authorize! :read, @user
      @site = current_user.site
      if session[:search_key].present?
        text =  session[:search_key]
        visit_ids = @site.visit_ids()
        size = 20
        from = params[:page].blank? ? 0 : params[:page]* size     #begin from page = 0
        if params[:type].blank? || params[:type] == 'note'
          @notes_search = Tire.search('notes',load: true) do
            from from
            size size
            query { string "text:#{text}" , :default_operator => 'AND'}
            filter :terms, :visit_id => visit_ids
            facet('visit_id') { terms :visit_ids }
          end
        end
        if params[:type].blank? || params[:type] == 'patient'
          @patients_search = Tire.search('patients',load: true) do
            from  from
            size  size
            query { string "name:#{text}" , :default_operator => 'AND'}
            filter :terms, :visit_ids => visit_ids
            facet('visit_ids') { terms :visit_ids }
          end
        end
      end
      respond_to do |format|
        format.html # index22.html.erb
        format.json { render json: :ok }
      end
  end
end
