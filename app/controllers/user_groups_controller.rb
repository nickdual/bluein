class UserGroupsController < ApplicationController
  include UserGroupsHelper
  before_filter :load_new_user_group, only: :create
  load_resource :site
  load_and_authorize_resource :user_group, through: :site
  layout 'site_chart', only: :index
  
  # GET /user_groups
  # GET /user_groups.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @user_groups }
    end
  end

  # GET /user_groups/1
  # GET /user_groups/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user_group }
    end
  end

  # GET /user_groups/new
  # GET /user_groups/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_group }
    end
  end

  # GET /user_groups/1/edit
  def edit
  end

  # POST /user_groups
  # POST /user_groups.json
  def create
    respond_to do |format|
      if @user_group.save
        format.html { redirect_to site_user_group_path(@site, @user_group), notice: 'User group was successfully created.' }
        format.json { render json: @user_group, status: :created, location: @user_group }
      else
        format.html { render action: "new" }
        format.json { render json: @user_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /user_groups/1
  # PUT /user_groups/1.json
  def update
    respond_to do |format|      
      if @user_group.update_attributes(get_group_params(params))
        format.html { redirect_to site_user_group_path(@site, @user_group), notice: 'User group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_groups/1
  # DELETE /user_groups/1.json
  def destroy
    @user_group.destroy

    respond_to do |format|
      format.html { redirect_to site_user_groups_path(@site) }
      format.json { head :no_content }
    end
  end
  
  private
  # Need to override CanCan's automatic loading here because we need to reformat the form's params
  def load_new_user_group
    @user_group = UserGroup.new(get_group_params(params))
  end
end
