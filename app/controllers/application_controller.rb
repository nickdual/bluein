class ApplicationController < ActionController::Base
  protect_from_forgery
  check_authorization 
  
  rescue_from CanCan::AccessDenied do |exception|
    if user_signed_in?
      redirect_to :back, :alert => exception.message
    else
      redirect_to new_user_session_path, :alert => exception.message
    end
  end
  
  private
  
  # Used by devise 
  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end
  
  def after_sign_in_path_for(resource_or_scope)
    home_path
  end
end
