class ContactsController < ApplicationController  
  load_resource :patient, instance_name: :contactable
  load_resource :user, instance_name: :contactable
  load_and_authorize_resource :contact, through: :contactable, singleton: true
  
  layout :contact_layout, only: :show

  # GET /patients/1/contact
  # GET /patients/1/contact.json
  def show
    @patient = @contactable if params[:patient_id].present?
    @user = @contactable if params[:user_id].present?

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contact }
    end
  end

  # GET /patients/1/contact/edit
  def edit
    if not @contact.persisted?
      @contact.save
    end
  end


  # PUT /patients/1/contact
  # PUT /patients/contact.json
  def update
    respond_to do |format|
      if @contact.update_attributes(params[:contact])
        format.html { redirect_to polymorphic_path(@contactable), notice: 'Contact was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
  def contact_layout
    if params[:patient_id].present?
      return "patient_chart"
    end
    if params[:site_id].present?
      return "site_chart"
    end
    if params[:user_id].present?
      return "user_chart"
    end
  end
end