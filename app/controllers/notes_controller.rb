class NotesController < ApplicationController
  layout 'patient_chart', only: [:index, :show]
  load_and_authorize_resource :patient
  load_and_authorize_resource :visit, except: :index
  load_and_authorize_resource :note, through: :visit, except: :index
  
  # GET /patients/1/notes
  # GET /patients/1/notes.json
  def index    
    if params[:visit_id].nil?
      @visit = @patient.current_visit
      if @visit.nil?
        @visit = @patient.visits.last
      end
    else
      @visit = Visit.find(params[:visit_id])
    end
    
    @notes = @visit.notes.accessible_by(current_ability) unless @visit.nil?
      
    authorize! :index, Note
      
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @notes }
    end
  end

  # GET /patients/1/visits/1/notes/1
  # GET /patients/1/visits/1/notes/1.json
  def show
    @author = User.find(@note.user_id) unless @note.user_id.nil?
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @note }
    end
  end

  # GET /patients/1/visits/1/notes/new
  # GET /patients/1/visits/1/notes/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @note }
    end
  end

  # GET /patients/1/visits/1/notes/1/edit
  def edit
  end

  # POST /patients/1/visits/1/notes
  # POST /patients/1/visits/1/notes.json
  def create
    @note.user_id = current_user._id
    respond_to do |format|
      if @note.save
        format.html { redirect_to patient_notes_path(@patient), notice: 'Note was successfully created.' }
        format.json { render json: @note, status: :created, location: @note }
      else
        format.html { render action: "new" }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /patients/1/visits/1/notes/1
  # PUT /patients/1/visits/1/notes/1.json
  def update
    respond_to do |format|
      if @note.update_attributes(params[:note])
        format.html { redirect_to patient_notes_path(@patient), notice: 'Note was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1/visits/1/notes/1
  # DELETE /patients/1/visits/1/notes/1.json
  def destroy
    @note.destroy

    respond_to do |format|
      format.html { redirect_to patient_notes_path(@patient) }
      format.json { head :no_content }
    end
  end
end
