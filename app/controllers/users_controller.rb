class UsersController < ApplicationController  
  layout 'user_chart', only: [:show, :login_record, :job_description]
  before_filter :load_new_user, only: :create
  load_and_authorize_resource
  
  # GET /users
  # GET /users.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @available_sites = @user.available_sites
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end
  
  # GET /users/1/login_record
  # GET /users/1/login_record.json
  def login_record
    respond_to do |format|
      format.html # login_record.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/job_description
  # GET /users/1/job_description.json
  def job_description
    @positions = @user.positions
    respond_to do |format|
      format.html # job_description.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user.site = current_user.site
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    attributes = get_user_params(params)
    
    respond_to do |format|
      if @user.update_attributes(attributes)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
  
  private
  def load_new_user
    @user = User.new(get_user_params(params))
  end
  
  def get_user_params(old_params)
    user_params = old_params[:user]
    user_params[:user_group_ids].delete("") unless user_params[:user_group_ids].nil?
    user_params.delete(:password) if user_params[:password].empty? unless user_params[:password].nil?
    
    if(!user_params[:site].nil? && user_params[:site].empty?)
      user_params[:site] = nil
    end
      
    user_params
  end
end
