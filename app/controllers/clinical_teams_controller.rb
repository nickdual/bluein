class ClinicalTeamsController < ApplicationController
  load_and_authorize_resource :clinical_team
  layout 'home_chart', only: [:index, :show]
  
  # GET /clinical_teams
  # GET /clinical_teams.json
  def index
    @site = current_user.site
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @clinical_teams }
    end
  end

  # GET /clinical_teams/1
  # GET /clinical_teams/1.json
  def show
    @site = current_user.site
    @positions = @clinical_team.positions 
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @clinical_team }
    end
  end

  # GET /clinical_teams/new
  # GET /clinical_teams/new.json
  def new
    if can? :manage, Site
      @sites_collection = Site.all
      @users_collection = User.all
    else
      @sites_collection = current_user.site
      @users_collection = current_user.site.users
    end
      
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @clinical_team }
    end
  end

  # GET /clinical_teams/1/edit
  def edit
    if can? :manage, Site
      @sites_collection = Site.all
      @users_collection = User.all
    else
      @sites_collection = current_user.site
      @users_collection = current_user.site.users
    end
  end

  # POST /clinical_teams
  # POST /clinical_teams.json
  def create
    respond_to do |format|
      if @clinical_team.save
        format.html { redirect_to @clinical_team, notice: 'Clinical team was successfully created.' }
        format.json { render json: @clinical_team, status: :created, location: @clinical_team }
      else
        format.html { render action: "new" }
        format.json { render json: @clinical_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /clinical_teams/1
  # PUT /clinical_teams/1.json
  def update
    respond_to do |format|
      if @clinical_team.update_attributes(params[:clinical_team])
        format.html { redirect_to @clinical_team, notice: 'Clinical team was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @clinical_team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clinical_teams/1
  # DELETE /clinical_teams/1.json
  def destroy
    @clinical_team.destroy

    respond_to do |format|
      format.html { redirect_to clinical_teams_url }
      format.json { head :no_content }
    end
  end
end
