class VisitsController < ApplicationController
  layout 'patient_chart', only: [:index, :show]
  load_resource :patient
  load_and_authorize_resource :visit, through: :patient
  
  # GET /patients/1/visits
  # GET /patients/1/visits.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @visits }
    end
  end

  # GET /visits/1
  # GET /visits/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @visit }
    end
  end

  # GET /visits/new
  # GET /visits/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @visit }
    end
  end

  # GET /visits/1/edit
  def edit
  end

  # POST /visits
  # POST /visits.json
  def create    
    respond_to do |format|
      if @visit.save
        format.html { redirect_to [@patient, @visit], notice: 'Visit was successfully created.' }
        format.json { render json: @visit, status: :created, location: @visit }
      else
        format.html { render action: "new" }
        format.json { render json: @visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /visits/1
  # PUT /visits/1.json
  def update
    respond_to do |format|
      if @visit.update_attributes(params[:visit])
        format.html { redirect_to [@patient, @visit], notice: 'Visit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /visits/1
  # DELETE /visits/1.json
  def destroy
    @visit.destroy

    respond_to do |format|
      format.html { redirect_to patient_visits_url(@patient) }
      format.json { head :no_content }
    end
  end
end
