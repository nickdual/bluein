class PhonesController < ApplicationController
  load_resource :patient, instance_name: :contactable
  load_resource :user, instance_name: :contactable
  load_and_authorize_resource :contact, through: :contactable, singleton: true
  load_and_authorize_resource :phone, through: :contact
  
  # GET /phones
  # GET /phones.json
  def index
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @phones }
    end
  end

  # GET /phones/1
  # GET /phones/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @address }
    end
  end

  # GET /phones/new
  # GET /phones/new.json
  def new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @address }
    end
  end

  # GET /phones/1/edit
  def edit
  end

  # POST /phones
  # POST /phones.json
  def create
    respond_to do |format|
      if @phone.save
        format.html { redirect_to polymorphic_path([@contactable, :contact]), notice: 'Phone was successfully created.' }
        format.json { render json: @address, status: :created, location: @address }
      else
        format.html { render action: "new" }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /phones/1
  # PUT /phones/1.json
  def update
    respond_to do |format|
      if @phone.update_attributes(params[:phone])
        format.html { redirect_to polymorphic_path([@contactable, :contact]), notice: 'Phone was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phones/1
  # DELETE /phones/1.json
  def destroy
    @phone.destroy

    respond_to do |format|
      format.html { redirect_to polymorphic_path([@contactable, :contact]), notice: 'Phone successfully deleted.' }
      format.json { head :no_content }
    end
  end
end
 