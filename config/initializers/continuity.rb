# application-wide constants
ROLES = [:bed_management, :billing, :coding, :imaging_orders, :lab_orders, 
  :metrics, :outpatient_bookings, :patient_admissions, :patient_records, :staff_rosters, :super_user, :user_admin]
