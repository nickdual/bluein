Continuity::Application.routes.draw do
  devise_for :users, controllers: { sessions: 'users/sessions' }

  resources :sites do
    resources :user_groups
    resources :locations
  end
  
  resources :clinical_teams do
    resources :positions
  end

  resources :users do
    resource :contact, only: [:show, :edit, :update] do #TODO: implement as concern in Rails 4
      resources :addresses, except: [:index, :show]
      resources :phones, except: [:index, :show]
    end
    member do
      get 'login_record'
      get 'job_description'
    end
  end

  resources :patients do
    resources :visits do
      resources :notes, except: :index
    end
    resources :notes, only: :index
    resource :contact, only: [:show, :edit, :update] do #TODO: implement as concern in Rails 4
      resources :addresses, except: [:index, :show]
      resources :phones, except: [:index, :show]
    end
    collection do
      get 'search'
    end
  end
  
  match 'home' => 'home#index', as: :home
  match '/home/suggestion' => 'home#suggestion'
  match '/home/show_more' => 'home#show_more'
  match 'stats' => 'home#stats', as: :stats

  root to: 'home#public'
end